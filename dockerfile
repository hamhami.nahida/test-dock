# Utiliser l'image de base Ubuntu
FROM ubuntu:latest

# Mettre à jour le système et installer Node.js
RUN apt-get update && apt-get install -y \
    nodejs \
    npm

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers de l'application dans le conteneur
COPY . .

# Installer les dépendances de l'application
RUN npm install

# Exposer le port sur lequel l'application tourne
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["npm", "start"]
