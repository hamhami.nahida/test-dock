<?php
// En-têtes pour autoriser les requêtes CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}

$input = file_get_contents('php://input');
$data = json_decode($input, true);

$directory = __DIR__ . '/csvFiles';

if (!file_exists($directory)) {
    mkdir($directory, 0777, true);
}

$fileName = 'PreInscription.csv';
$filePath = $directory . '/' . $fileName;

$csvFile = fopen($filePath, 'a');

if ($csvFile === false) {
    http_response_code(500);
    echo json_encode(['error' => 'Could not open file for writing']);
    exit();
}

$headers = ['Nom', 'Prénom', 'Téléphone', 'Email', 'Formation', 'Intéressé par', "J'ai connu l'IPSSI grâce"];
if (filesize($filePath) === 0) {
    fputcsv($csvFile, $headers);
}

$date= (new DateTime())->format('Y-m-d H:i:s');

$dataRow = [
    $data['nom'],
    $data['prenom'],
    $data['telephone'],
    $data['email'],
    $data['formation'],
    $date,
    implode('|', $data['options1']),
    implode('|', $data['options2'])
];

fputcsv($csvFile, $dataRow);
fclose($csvFile);

http_response_code(200);
echo json_encode(['success' => 'Data saved successfully']);
?>
